import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { Fixture } from 'src/app/models/fixture';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit, OnDestroy {
  @Input() tableData$: Observable<Fixture[]>;

  displayedColumns: string[] = ['fixtureName', 'kickoff', 'venue'];
  subscription: Subscription;
  noData = false;

  constructor() { }


  /**
   * on init
   */
  ngOnInit(): void {
    this.subscription = this.tableData$.subscribe((data: Fixture[]) => {
      this.noData = data.length === 0;
    });
  }


  /**
   * on destroy
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
