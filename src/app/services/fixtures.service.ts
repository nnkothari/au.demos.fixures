import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Fixture } from '../models/fixture';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FixturesService {

  /**
   * Creates an instance of fixtures service.
   * @param http instance of http client service
   */
  constructor(private http: HttpClient) { }

  /**
   * Gets fixtures list
   * @param searchTerm string term to be searched. Ex. 'arsenal'
   * @returns array of type fixtures
   */
  getFixtures(searchTerm: string): Observable<Fixture[]> {
    return this.http.get<Fixture[]>(`${environment.origin}Fixtures?searchTerm=${searchTerm}&count=1000&skip=0`);
  }

}
