import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Fixture } from 'src/app/models/fixture';
import { Observable } from 'rxjs';
import { FixturesService } from 'src/app/services/fixtures.service';
import { FormControl } from '@angular/forms';
import { switchMap, debounceTime, mergeMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit {

  searchResults$: Observable<Fixture[]>;
  searchField = new FormControl();
  isLoading = false;

  /**
   * Creates an instance of search page component.
   * @param fixturesService instance of FixturesService service
   */
  constructor(private fixturesService: FixturesService) { }

  /**
   * on init
   */
  ngOnInit() {
    this.searchResults$ = this.searchField.valueChanges.pipe(
      tap(() => this.isLoading = true),
      debounceTime(300),
      switchMap(val => this.fixturesService.getFixtures(val)),
      tap(() => this.isLoading = false));
  }
}
